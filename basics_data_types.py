day_hours = 24
week_days = 7

week_hours = day_hours * week_days
print(week_hours)

#integer
x = 10
#string
y = '10'
#float
z = 10.1

sum1 = x + x
sum2 = y + y
print(sum1, sum2)
print(type(x), type(y), type(z))

#lists & dictionaries
monday_temp = [9.1, 8.8, 7.5]
student_grades = {"Mary": 9.1, "Sim": 8.8, "John": 7.5}
print(student_grades)
xy = list(range(1, 10))
print(xy)
xyz = list(range(1,10,2))
print(xyz)
student_grades["Sim"]
print(student_grades)

#total
mysum = sum(student_grades.values())
#number of items in list
length = len(student_grades)
#divide total/number
mean = mysum / length
print(mean)
print(student_grades.keys())

#tuples
tuesday_temp = (1, 4, 5)
print(tuesday_temp)

#
monday_temp = [9.1, 9.8, 7.5]
monday_temp.append(8.1)
print(monday_temp)

#monday_temp.clear()
#print(monday_temp)

#indexing
monday_temp.index(8.8)
print(monday_temp)
monday_temp[1]
print(monday_temp)
monday_temp[:3]
print(monday_temp)
monday_temp[1:2]
print(monday_temp)
monday_temp[-1]
print(monday_temp)
monday_temp[-2:]
print(monday_temp)
monday_temp[-5:-2]
print(monday_temp)

